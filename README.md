# Qemu Packer builder for Debian

The Qemu Packer builder is able to create KVM virtual machine images. In this example, we build virtual machine images in the qcow2 format for debian ISO.




## Usage

```bash
$ packer build debian10.json

or

$ packer build -var-file="config.json" debian10.json

$ cat config.json
{
  "password": "<PASS>",
  "user": "<USER>"
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[Apache License](http://www.apache.org/licenses/)
